import org.rspeer.RSPeer;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.GrandExchange;
import org.rspeer.runetek.api.component.GrandExchangeSetup;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.LoopTask;

import java.util.Random;
import java.util.function.Predicate;

@ScriptMeta(developer = "DrakenScripts", desc = "Crushes Bird nests", name = "DS Bird Nest Crusher")
public class BirdCrusher extends Script {
    static String BIRD_NEST_NAME = "Bird nest";
    static String MORTAR_NAME = "Pestle and mortar";
    private static final Predicate<Item> BIRD_NEST_PREDICATE = item -> item.getName().equals(BIRD_NEST_NAME);
    private static final Predicate<Item> MORTAR_PREDICATE = item -> item.getName().equals(MORTAR_NAME);

    Item[] mortars, nests;
    void UpdateSupplies(){
        mortars = Inventory.getItems(MORTAR_PREDICATE);
        nests = Inventory.getItems(BIRD_NEST_PREDICATE);
    }
    Random r = new Random();

    @Override
    public int loop() {
        UpdateSupplies();
        //int expectedCount = nests.length;
        if (nests.length > 0 && mortars.length > 0)
        {
            Item lastNest = nests[nests.length - 1];
            Item mortar = mortars[0];
            mortar.interact("Use");
            Time.sleep(22, 160);
            if (r.nextFloat()>0.9)
                Time.sleep(200, 1100);
            lastNest.interact("Use");
            Time.sleep(22, 160);
            if (r.nextFloat()>0.9)
               Time.sleep(200, 500);
            //Time.sleep(22, 89);
            //lastNest.click();
            return 10;
        }
        else return  GetSupplies();
    }
    int GetSupplies(){
        Time.sleep(200, 1100);
        if(!Bank.isOpen()) {
            Bank.open();
            Time.sleep(100, 500);
            if(this.isStopping()) return 0;
        }
        if (mortars.length > 0 && mortars[0].getIndex() != 27)
            Bank.depositInventory();
        else
            Bank.depositAllExcept(MORTAR_PREDICATE);
        if(this.isStopping()) return 0;
        if(Bank.getCount(BIRD_NEST_PREDICATE) == 0)
        {
            Time.sleep(200, 2000);
            if(this.isStopping()) return 0;
            Bank.close();
            Time.sleep(200, 2000);
            if(this.isStopping()) return 0;
            Npc clerk = Npcs.getNearest("Grand Exchange Clerk");
            clerk.interact("Exchange");
            Time.sleep(600, 2000);
            if(this.isStopping()) return 0;
            GrandExchange.collectAll();
            Time.sleep(200, 2000);
            if(this.isStopping()) return 0;
            Bank.open();
            Time.sleep(700, 2000);
            if(this.isStopping()) return 0;
            Bank.depositAllExcept(MORTAR_PREDICATE);
            Time.sleep(100, 300);
            if(this.isStopping()) return 0;
            if(Bank.getCount(BIRD_NEST_PREDICATE) == 0) {
                this.setStopping(true);
                return 0;
            }
            return 1;
        }
        Time.sleep(408, 3100);
        if(this.isStopping()) return 0;
        Bank.withdraw(BIRD_NEST_PREDICATE, 27);
        Time.sleep(303, 2100);
        if(this.isStopping()) return 0;
        Bank.withdraw(MORTAR_PREDICATE, 1);
        Time.sleep(404, 3100);
        if(this.isStopping()) return 0;
        Bank.close();
        if(this.isStopping()) return 0;
        Time.sleep(300, 2500);
        return 10;
    }

    public void onStart(){

    }
    public void onStop(){

    }
}